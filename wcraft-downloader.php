#!/usr/bin/env php
<?php

use App\WDownloader;

require_once 'app/WDownloader.php';
require_once 'vendor/autoload.php';

//Tracy\Debugger::enable(false);

$resolutionAliases = [
    'hd' => '1280x720',
    '720p' => '1280x720',
    '1080p' => '1920x1080',
    'fullhd' => '1920x1080',
    '4k' => '3840x2160',
];

if (isset($argv[1]) && in_array($argv[1], ['-h', '--help'], true)) {    
    echo "Usage <category> <resolution> <output>\n";
    exit;
}

$category = $argv[1] ?? 'all';
$resolution = $argv[2] ?? '1920x1080';
if (isset($resolutionAliases[$resolution])) {
    $resolution = $resolutionAliases[$resolution];
}
$targetDir = $argv[3] ?? './!download/'.$category.'/'.$resolution;

$url = 'https://wallpaperscraft.com';
if (str_starts_with($category, 'all')) {
    $url .= '/'.$category.'/'.$resolution;
} else {
    $url .= '/catalog/'.$category.'/'.$resolution;
}

$downloader = new WDownloader($targetDir);
for ($page = 1; $page <= 50; $page++) {
    $downloader->downloadList($url.'/page'.$page);
}
