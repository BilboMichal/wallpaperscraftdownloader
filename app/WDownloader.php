<?php

namespace App;

use DOMDocument;
use DOMXPath;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\RequestOptions;
use Nette\Utils\FileSystem;
use Psr\Http\Message\StreamInterface;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
class WDownloader
{
    const REQUEST_DELAY = 1000;
    const SLEEP_SECONDS = 5;

    /** @var Client */
    private $client;

    /** @var string */
    private $targetDir;

    public function __construct(string $targetDir)
    {
        $this->targetDir = $targetDir;
    }

    public function getClient(): Client
    {
        if (!isset($this->client)) {
            $this->client = new Client;
        }

        return $this->client;
    }

    public function setClient(Client $client)
    {
        $this->client = $client;
        return $this;
    }

    private function request($url): StreamInterface
    {
        while (1) {
            try {
                $source = $this->getClient()->get($url, [
                    RequestOptions::DELAY => self::REQUEST_DELAY,
                ])->getBody();
            } catch (ClientException $ex) {
                if ($ex->getCode() === 429) {
                    echo sprintf("Too many requests, going to sleep for %d seconds...\n", self::SLEEP_SECONDS);
                    sleep(self::SLEEP_SECONDS);
                    continue;
                } else {
                    throw $ex;
                }
            }
            break;
        }

        return $source;
    }

    public function downloadItem(string $url)
    {
        $html = $this->request($url)->getContents();
        $item = self::parseHtml($html)->query('//a[@class="gui-button gui-button_full-height"]')->item(0);
        if (!$item) {
            return;
        }

        $imageUrl = (string) $item->getAttribute('href');
        FileSystem::createDir($this->targetDir);
        $targetFile = $this->targetDir.'/'.basename($imageUrl);

        $target = fopen($targetFile, 'w');
        echo "Downloading '".$imageUrl."'";
        $source = $this->request($imageUrl);
        $result = stream_copy_to_stream($source->detach(), $target);
        fclose($target);

        echo $result ? " OK\n" : " FAIL!\n";
        return $result;
    }

    public function downloadList(string $url)
    {
        $urlParsed = parse_url($url);
        $domain = $urlParsed['scheme'].'://'.$urlParsed['host'];

        echo "Downloading list '".$url."'\n";
        $html = $this->request($url)->getContents();
        $items = self::parseHtml($html)->query('//a[@class="wallpapers__link"]');
        foreach ($items as $item) {
            $itemUrl = $domain.$item->getAttribute('href');
            $this->downloadItem($itemUrl);
        }

        echo "---------------------------------\n";
    }

    protected static function parseHtml(string $html): DOMXPath
    {
        $dom = new DOMDocument('1.0', 'UTF-8');
        @$dom->loadHTML($html);
        $xpath = new DOMXPath($dom);

        return $xpath;
    }
}